function handle_1() {
  const diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  const khuVuc = document.getElementById("txt-khu-vuc").value * 1;
  const doiTuong = document.getElementById("txt-doi-tuong").value * 1;
  const diemToan = document.getElementById("txt-diem-toan").value * 1;
  const diemVan = document.getElementById("txt-diem-van").value * 1;
  const diemTuChon = document.getElementById("txt-diem-tu-chon").value * 1;
  const ketQua = document.getElementById("result1");
  var tongDiem = 0;
  if (
    diemToan >= 0 &&
    diemVan >= 0 &&
    diemTuChon >= 0 &&
    diemToan <= 10 &&
    diemVan <= 10 &&
    diemTuChon <= 10
  ) {
    if (diemToan == 0 || diemVan == 0 || diemTuChon == 0) {
      ketQua.innerHTML = `<h1> Thí sinh trượt xét tuyển do có điểm liệt</h1>`;
    } else {
      tongDiem = diemToan + diemVan + diemTuChon + khuVuc + doiTuong;
      if (tongDiem >= diemChuan) {
        ketQua.innerHTML = `<h1> Thí sinh đậu xét tuyển, tổng điêm: ${tongDiem.toFixed(
          2
        )}</h1>`;
      } else {
        ketQua.innerHTML = `<h1> Thí sinh trượt xét tuyển, tổng điêm: ${tongDiem.toFixed(
          2
        )}</h1>`;
      }
    }
  } else {
    ketQua.innerHTML = `<h1> Nhập lại</h1>`;
  }
}

function handle_2() {
  const tenEl = document.getElementById("txt-ten").value;
  const kwEl = document.getElementById("txt-kw").value * 1;
  const ketQua = document.getElementById("result2");
  var tongTien = 0;
  if (kwEl < 0) {
    ketQua.innerHTML = `<h1> Nhập lại </h1>`;
  } else if (kwEl <= 50) {
    tongTien = kwEl * 500;
    ketQua.innerHTML = `<h1> Họ và tên ${tenEl}, số tiền ${tongTien.toLocaleString()} vnd </h1>`;
  } else if (kwEl > 50 && kwEl <= 100) {
    tongTien = 50 * 500 + (kwEl - 50) * 650;
    ketQua.innerHTML = `<h1> Họ và tên ${tenEl}, số tiền ${tongTien.toLocaleString()} vnd </h1>`;
  } else if (kwEl > 100 && kwEl <= 200) {
    tongTien = 50 * 500 + 50 * 650 + (kwEl - 100) * 850;
    ketQua.innerHTML = `<h1> Họ và tên ${tenEl}, số tiền ${tongTien.toLocaleString()} vnd </h1>`;
  } else if (kwEl > 200 && kwEl <= 350) {
    tongTien = 50 * 500 + 50 * 650 + 100 * 850 + (kwEl - 200) * 1100;
    ketQua.innerHTML = `<h1> Họ và tên ${tenEl}, số tiền ${tongTien.toLocaleString()} vnd </h1>`;
  } else {
    tongTien =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kwEl - 350) * 1300;
    ketQua.innerHTML = `<h1> Họ và tên ${tenEl}, số tiền ${tongTien.toLocaleString()} vnd </h1>`;
  }
}
